# Arbeitsprotokoll

## Auftrag 1

Ich habe mir die beiden Seiten durchgelesen und gemerkt, dass es sowohl etwas gibt was ich schon habe, was
ich machen werde wollen und was ich nicht machen will.

Es ist so ich habe auf meinem Laptop bereits Admin, User und BIOS Paswörter festgelegt. Dabei ist es aber auch so,
dass wen man mit dem User Passwort reinkommt und dann nichts tut, öffnet sich ein linuxbasiertes Betriebssystem.
Dieses hat selber auch ein Etwas stärkers Passwort definiert und würde man irgendetwas machen wollen so muss man
auch wieder ein Passwort eingeben.

Ich werde den Tipp mit den Farben und Aufklebern versuchen. Dies heist dass ich etwas wie man mich erreichen 
kann mit drauf tun werde. Aber auch dass ich etwas auffäligere Farben benutzen werde dmit mann schneller
es schneller erkennt.

Ich werde allerdings warscheinlich nicht so oft die GPS-Ortung einschallten da ich dies von mir selber aus
nicht so gerne tuhe.

Eventuell werde ich den tipp mit dem verschlüsseln von Daten einsetzen.

## Auftrag 2

Ich habe mir VeraCrypt angeschaut und finde dass ich es eventuel verwenden werde.

### Wie verwendet man VeraCrypt?

VeraCrypt ist relativ einfach zu bedinen und folgt man den Anweisungen so hat man es auch sehr einfach geschafft
einen Container zu erstellen. Will man auf diesen zugreifen so muss dieser in VeraCrypt geöffnet werden.
Dazu fügt man die datei einem der VeraCrypt laufwrerke hinzu und dann kann man auch weitere dateien in diesen Container hinzufügen.

### Vorteile von VeraCrypt

VeraCrypt kann (sovern ein starkes Passwortverwendet wird) Container Stark verschlüsseln und damit auch die sicherheit dieser Dateien auch
stark erhöhen

## Meine Meinung und Erfahrung mit VeraCrypt

Ich finde es kann gut sein aber das problem wäre, wenn man dass Passwort vergisst.
Dies kann vorallem dann ein Problem werden wenn man Firmendateien mit solch einem Tool gesichert hat.
Beispielsweise dann wenn man einen Unfall hatte und einer der Kollegen muss an etwas weiterarbeiten was so gesichert wurde.

Ich habe damit auch bereits zwei Container erstellt. Einer von diesen zwei wird dazu noch als eine
datei von einem Programm angezeigt. So kann man in VeraCrypt einen Verschlüsselten Container mit der endung .pptx erstellen,
dieser wird dann auch als PowerPoint datei angezeig. Versucht man allerdings diesen wieder zu öffnen, dann wird eine beschädighte - nicht 
reparierbare datei erstellt
